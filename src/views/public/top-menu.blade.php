<div id="main-nav" class="navbar navbar-default navbar-static-top yamm sticky" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="" href="/">
                @if(($show_logo_in_menubar == 1) && ($site_logo != ''))
                    <img src="/storage/site_files/{!! $site_logo !!}" alt="" class="img img-responsive logo-in-top-menu" style="max-height: 50px;">
                @endif
            </a>
        </div>

        <div class="navbar-collapse collapse">
            <ul id="list-social-icons" class="nav navbar-nav social-icons icon-circle">
                <li><a style="margin-left: 9px;" href="https://kp.ignitefredericton.com/" target="_blank" class="social-icon-sm si-gray-round"><img src="/storage/photos/shares/icons/kp.png" class="img img-responsive" alt="Knowledge Park" style="max-height: 25px; margin-left: -12px; margin-top: -14px;"></a></li>
                <li><a style="margin-left: 9px;" href="https://ignitefredericton.com/" target="_blank" class="social-icon-sm si-gray-round"><img src="/storage/photos/shares/icons/if.png" class="img img-responsive" alt="Ignite Fredericton" style="max-height: 25px; margin-left: -7px; margin-top: -12px;"></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if((Auth::check()) && (Auth::user()->access_level == 3))
                    @foreach($menu as $item)
                        @if ($item->has_children == 0)
                            @if ($item->active == 1)
                                @if ($item->page_id == 0)
                                    <li>
                                        <a class='mitem' data-mitem-id="{!! $item->id !!}" href='{!! $item->url !!}' @if($item->new_window == 1) target="_blank" @endif>
                                            {!! $item->{"menu_text_" . App::getLocale()} !!}
                                        </a>
                                    </li>
                                @else
                                    <li>
                                        <a class='mitem' data-mitem-id="{!! $item->id !!}" href='/{!! $item->targetPage->slug !!}' @if($item->new_window == 1) target="_blank" @endif>
                                            {!! $item->{"menu_text_" . App::getLocale()} !!}
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if ($item->page_id == 0)
                                    <li>
                                        <a class='mitem' data-mitem-id="{!! $item->id !!}" href='{!! $item->url !!}' @if($item->new_window == 1) target="_blank" @endif>
                                            <em class="text-warning">{!! $item->{"menu_text_" . App::getLocale()} !!}</em>
                                        </a>
                                    </li>
                                @else
                                    <li>
                                        <a class='mitem' data-mitem-id="{!! $item->id !!}" href='/{!! $item->targetPage->slug !!}' @if($item->new_window == 1) target="_blank" @endif>
                                            <em class="text-warning">{!! $item->{"menu_text_" . App::getLocale()} !!}</em>
                                        </a>
                                    </li>
                                @endif
                            @endif
                        @else
                            @if ($item->active == 1)
                                <li class="dropdown">
                                    <a href="#" class="mitem dropdown-toggle"
                                       data-mitem-id="{!! $item->id !!}"
                                       data-toggle="dropdown" role="button" aria-expanded="false">
                                        {!! $item->{"menu_text_" . App::getLocale()} !!}
                                        <span class="caret"></span>
                                    </a>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="mitem dropdown-toggle"
                                       data-mitem-id="{!! $item->id !!}"
                                       data-toggle="dropdown" role="button" aria-expanded="false">
                                        <em class="text-warning">{!! $item->{"menu_text_" . App::getLocale()} !!}</em>
                                        <span class="caret"></span>
                                    </a>
                                    @endif
                                    <ul class="dropdown-menu" role="menu">
                                        @foreach ($item->dropdownItems as $dd)
                                            @if ($dd->active == 1)
                                                @if ($dd->page_id == 0)
                                                    <li>
                                                        <a class='ddmitem' data-ddmitem-id="{!! $dd->id !!}" @if($dd->new_window == 1) target="_blank" @endif
                                                        data-mitem-id="{!! $item->id !!}" href="{!! $dd->url !!}">
                                                            {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                        </a>
                                                    </li>
                                                @else
                                                    <li>
                                                        <a class='ddmitem' data-ddmitem-id="{!! $dd->id !!}" @if($dd->new_window == 1) target="_blank" @endif
                                                        data-mitem-id="{!! $item->id !!}" href="/{!! $dd->targetPage->slug !!}">
                                                            {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                        </a>
                                                    </li>
                                                @endif
                                            @else
                                                @if ($dd->page_id == 0)
                                                    <li>
                                                        <a class="ddmitem" data-ddmitem-id="{!! $dd->id !!}" @if($dd->new_window == 1) target="_blank" @endif
                                                        data-mitem-id="{!! $item->id !!}" href="{!! $dd->url !!}">
                                                            <em class='text-warning'>
                                                                {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                            </em>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li>
                                                        <a class="ddmitem" data-ddmitem-id="{!! $dd->id !!}" @if($dd->new_window == 1) target="_blank" @endif
                                                        data-mitem-id="{!! $item->id !!}" href="/{!! $dd->targetPage->slug !!}">
                                                            <em class='text-warning'>
                                                                {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                            </em>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                        <li><a href='#' onclick="addDDMenuItem(<?= $item->id ?>)">[Add item]</a></li>
                                    </ul>
                            @endif
                            @endforeach
                            @else
                                @foreach($menu as $item)
                                    @if ($item->has_children == 0)
                                        @if ($item->page_id == 0)
                                            <li>
                                                <a href='{!! $item->url !!}' @if($item->new_window == 1) target="_blank" @endif>
                                                    {!! $item->{"menu_text_" . App::getLocale()} !!}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href='/{!! $item->targetPage->slug !!}' @if($item->new_window == 1) target="_blank" @endif>
                                                    {!! $item->{"menu_text_" . App::getLocale()} !!}
                                                </a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle"
                                               data-toggle="dropdown" role="button" aria-expanded="false">
                                                {!! $item->{"menu_text_" . App::getLocale()} !!}
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                @foreach ($item->dropdownItems as $dd)
                                                    @if ($dd->active == 1)
                                                        @if ($dd->page_id == 0)
                                                            <li>
                                                                <a href="{!! $dd->url !!}"  @if($dd->new_window == 1) target="_blank" @endif>
                                                                    {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li>
                                                                <a href="/{!! $dd->targetPage->slug !!}"  @if($dd->new_window == 1) target="_blank" @endif>
                                                                    {!! $dd->{"menu_text_" . App::getLocale()} !!}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                    @endif
                                @endforeach
                            @endif


                            @if((Auth::check()) && (Auth::user()->access_level == 3))
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-wrench"></i>
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="/admin/dashboard">Dashboard</a></li>
                                        @if(Auth::user()->hasRole('pages'))
                                            @if((isset($page->id)) && ($page->id > 0))
                                                <li id="editon"><a href="#!" class="menu-item" onclick="makePageEditableCJS()">Edit page</a></li>
                                            @endif
                                            @if((isset($post_id)) && ($post_id > 0))
                                                <li id="editon"><a href="#!" class="menu-item" onclick="makePageEditableCJS()">Edit page</a></li>
                                            @endif
                                            @if((isset($news_id)) && ($news_id > 0))
                                                <li id="editon"><a href="#!" class="menu-item" onclick="makePageEditableCJS()">Edit page</a></li>
                                            @endif
                                            @if((isset($event_id)) && ($event_id > 0))
                                                <li id="editon"><a href="#!" class="menu-item" onclick="makePageEditableCJS()">Edit page</a></li>
                                            @endif
                                            <li><a href="#!" onclick="addSitePage()">Add Page</a></li>
                                        @endif
                                        @if(Auth::user()->hasRole('blogs'))
                                            <li><a href="#!" onclick="addBlogPost()">Add Blog Post</a></li>
                                        @endif
                                        @if(Auth::user()->hasRole('news'))
                                            <li><a href="/admin/new-news-item">Add News Item</a></li>
                                        @endif
                                        <li><a href="#!" onclick="addMenuItem()">Add Menu Item</a></li>
                                        <li><a href="#!" onclick="flushCache()">Flush Application cache</a></li>
                                        <li><a href="/user/logout">Logout</a></li>
                                    </ul>
                                </li>
                            @endif

                            <li class="dropdown " data-animate="animated fadeInUp" style="z-index:500;">
                                <a href="#" class="dropdown-toggle " data-toggle="dropdown"><i class="fa fa-search"></i></a>
                                <ul class="dropdown-menu search-dropdown animated fadeInUp">
                                    <li id="dropdownForm">
                                        <div class="dropdown-form">
                                            {!! Form::open(array('url' => '/search', 'class' => 'form-inline', 'method' => 'post')) !!}
                                            <div class="input-group">
                                                {!! Form::text('q', null, array('class' => 'form-control', 'placeholder' => 'search...')) !!}
                                                <span class="input-group-btn">
                                                <input type="submit" class="btn btn-theme-bg" value="Go!">
                                            </span>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </li>
                                </ul>
                            </li>
            </ul>
        </div>
    </div>
</div>
